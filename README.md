![alt text](https://gitlab.com/fgallese/itau-slack/raw/2e3778ae84cf9fc0724c4c6191b31d6737c6169c/img/brand.gif "Logo Title")
# ITAU-Slack Connector

## Descripción

Este programa se conecta a la web del banco ITAU Uruguay, se autentica con el par usuario/contraseña previamente configurados, y obtiene los datos de las cuentas.
Si detecta un cambio con respecto al último chequeo, envía una notificación a través de Slack usando un incoming webhook.

Automáticamente se detectan todos los tipos de cuenta (caja de ahorros/cuenta corriente/otros) y se lleva un registro independiente del estado de las mismas.

TODO: Cuando hay un cambio en el saldo, aportar información sobre las transacciones que generaron ese cambio.

## Requisitos

Python 2.7
slackweb

## Instalación

Simplemente instalar el conector de Slack para Python:

`# pip install slackweb`

Y descargar el script de este repo. Luego, cronear la ejecución del mismo. Por ejemplo:

`*/5 * * * * /usr/bin/python /opt/itau-slack/itau-slack.py`

## Configuración

Dentro del script se deben setear 3 variables:

`usr = 'usuario-itau'
pwd = 'passwd-itau'
slack_hook = 'incomming webhook de slack'`

## Ejemplo

Cuando se detecta un decremento en el saldo de una cuenta, el mensaje se publica en color rojo:

![alt text](https://gitlab.com/fgallese/itau-slack/raw/7480e90ef766a9d0b00ebeb64e3a3a17ae542415/img/example1.png "Ejemplo 1")

En cambio, cuando hay un aumento en el saldo de una cuenta, el mensaje es verde:

![alt text](https://gitlab.com/fgallese/itau-slack/raw/7480e90ef766a9d0b00ebeb64e3a3a17ae542415/img/example2.png "Ejemplo 2")
