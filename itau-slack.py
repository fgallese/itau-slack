#!/usr/bin/python

import pycurl, urllib, json, slackweb, os.path
from StringIO import StringIO

usr = ''
pwd = ''
slack_hook = ''
login_url = 'https://www.itaulink.com.uy/trx/doLogin'

data = {
	'segmento': 'panelPersona',
	'empresa_aux': usr,
	'pwd_empresa': pwd,
	'usuario_aux': '',
	'tipo_documento': 1,
	'nro_documento': usr,
	'pass': pwd,
	'password': pwd,
	'pwd_usuario': '',
	'empresa': '',
	'usuario': '',
	'id': 'login',
	'tipo_usuario': 'R'
}

headers = [
	'Origin: https://www.itau.com.uy',
	'Accept-Encoding: gzip, deflate, br',
	'Accept-Language: es-ES,es;q=0.8,en;q=0.6,pt;q=0.4',
	'Upgrade-Insecure-Requests: 1',
	'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'Content-Type: application/x-www-form-urlencoded',
	'Cache-Control: max-age=0',
	'Referer: https://www.itau.com.uy/inst/',
	'Connection: keep-alive'
]

login_data = urllib.urlencode(data)

crate = StringIO()

c = pycurl.Curl()
c.setopt(c.URL, login_url)
c.setopt(pycurl.TIMEOUT, 10)
c.setopt(pycurl.FOLLOWLOCATION, 1)
c.setopt(pycurl.POSTFIELDS, login_data)
c.setopt(pycurl.COOKIEJAR, 'cookies')
c.setopt(pycurl.COOKIEFILE, 'cookies')
c.setopt(pycurl.USERAGENT, 'Get Accounts Info (https://gitlab.com/fgallese/itau-slack)')
c.setopt(pycurl.HTTPHEADER, headers)
c.setopt(c.WRITEFUNCTION, crate.write)
#c.setopt(c.VERBOSE, True)

c.perform()

for line in crate.getvalue().splitlines():
	if "var mensajeUsuario" in line:
		gold = line

crate.close()
slack = slackweb.Slack(url=slack_hook)
attachments = []

qdata = json.loads(gold.split('\'')[1])

cambio = False

for cuenta in qdata['cuentas']:
	for ca in qdata['cuentas'][cuenta]:
		cambio_c = False
		filename = '/tmp/itau-slack.' + str(ca['idCuenta'])
		if not os.path.isfile(filename):
			with open(filename, 'w') as file:
				file.write("0")
		with open(filename, 'r') as file:
			saldo = file.readline()
		if float(saldo) > float(ca['saldo']):
			color = '#ff0000'
			cambio_c = cambio = True
		elif float(saldo) < float(ca['saldo']):
			color = '#36a64f'
			cambio_c = cambio = True
		if cambio_c:
			cta = 'Cuenta' + ' ' + str(ca['idCuenta']) + ' (' + cuenta + ')'
			attachment = {
				"color": color,
				"title": cta,
				"pretext": "Ha habido un movimiento en su cuenta.",
				"text": "Estos son los datos actuales de su cuenta.",
				"mrkdwn_in": [
					"text",
					"pretext"],
				"fields": [
					{
						"title": "Saldo",
						"value": str(ca['saldo']),
						"short": "true"
					},
					{
						"title": "Titular",
						"value": str(ca['nombreTitular']),
						"short": "true"
					},
					{
						"title": "Moneda",
						"value": str(ca['moneda']),
						"short": "true"
					},
					{
						"title": "Diferencia",
						"value": str(float(ca['saldo']) - float(saldo)),
						"short": "true"
					}
				]
			}
			attachments.append(attachment)
			with open(filename, 'w') as file:
				file.write(str(ca['saldo']))

if cambio:
	slack.notify(attachments=attachments)

c.close()
